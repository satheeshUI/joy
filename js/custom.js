$( function() {

    let start = document.getElementsByClassName("informationSection__steppers__start")[0]
    let content = document.getElementsByClassName("informationSection__steppers__content")[0]
    $(start).show()
    $(content).hide() 

  if(window.innerWidth < 992) {
    let hdr = document.getElementsByTagName('header')[0]
    let main = document.getElementsByTagName('main')[0]
    main.style.paddingTop = hdr.clientHeight + 'px'
  }
  

    var projects = [
      {
        value: "Product 1",
        label: "Product 1",
        icon: "product.jpg",
        price: 450,
        offerPrice: 399
      },
      {
        value: "Product 2",
        label: "Product 2",
        icon: "product.jpg",
        price: "450",
        offerPrice: "399"
      },
      {
        value: "Product 3",
        label: "Product 3",
        icon: "product.jpg",
        price: "450",
        offerPrice: "399"
      }
    ];
 
    $( "#project" ).autocomplete({
      minLength: 0,
      source: projects,
      focus: function( event, ui ) {
        $( "#project" ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#project" ).val( ui.item.label );
        $( "#project-id" ).val( ui.item.value );
        $( "#project-description" ).html( ui.item.desc );
        // $( "#project-icon" ).attr( "src", "images/" + ui.item.icon );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append("<div class='d-flex'>" + 
            "<div class='product-img'>" + "<img src='./images/" + item.icon + "'/>" + "</div>" +
            "<div class='product-desc'>" + item.label + "</div>" +
            "<div class='product-price'>" + 
                "<label>₪" + item.offerPrice + "</label>" +
                "<label class='strike'>₪" + item.price + "</label>"
            + "</div>"
        + "</div>")
        .appendTo( ul );
    };

    // banner video
    let ele = $('.bannervideo1')[0]
    if(ele && ele.length) {
      let elesrc = ele.attributes
      let ele1 = $('.bannervideo2')[0]
      let elesrc1 = ele1.attributes
      let ele2 = $('.bannervideo3')[0]
      let elesrc2 = ele2.attributes
      
        new $.fn.J_video_background({
          $el: $('.bannervideo1'),
          src: elesrc['src'].value,
        });
        new $.fn.J_video_background({
          $el: $('.bannervideo2'),
          src: elesrc['src'].value,
        });
        new $.fn.J_video_background({
          $el: $('.bannervideo3'),
          src: elesrc['src'].value,
        });
    }
      
      $('.productSection__carousel, .bestSellers__carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
        dots: false,
        responsive: {
          0: {
            items: 2,
            nav: false,
            dots: true
          },
          600: {
            items: 2,
            nav: false,
            dots: true
          },
          991: {
            items: 4,
            margin: 20
          },
          1600: {
            items: 5,
            margin: 30
          }
        }
      })

      $('.insta__carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: true,
        dots: false,
        responsive: {
          0: {
            items: 2,
            nav: false,
            dots: true
          },
          600: {
            items: 2,
            nav: false,
            dots: true
          },
          991: {
            items: 4,
            margin: 22
          }
        }
      })

      $('.completmentaryCarousel').owlCarousel({
        items: 2,
        loop: true,
        dots: true,
        margin: 15
      })

      $('.productCoursel').owlCarousel({
        items: 1,
        loop: true,
        dots: true
      })
      $('.banner-carousel, .banner-carousel1, .banner-carousel2').owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        nav: true
      })
      
      $(document).mouseup(function(e) {
        let item = document.getElementsByClassName('sz-navbar-inner')[0]
        if ($(e.target).closest(".sz-navbar-items").length === 0) { 
          if($(item).hasClass('active')) {
            $(item).removeClass('active')
            $('body').css('overflow-y', 'auto')
          }
        }
      })

      let main = document.getElementsByTagName("main")[0]
      let parentnode = document.createElement("div")
      parentnode.className = "scrolltop"
      var textnode = document.createElement("i");
      textnode.className = "fa fa-chevron-up"
      parentnode.appendChild(textnode)
      main.appendChild(parentnode)
      parentnode.addEventListener("click", function() {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false; 
      })

    } );


    window.onscroll = function() {scrollShrink()}
    let scrollAmount = 120
    function scrollShrink() {
      let scrolltopbtn = document.getElementsByClassName("scrolltop")[0]
      if(document.body.scrollTop > scrollAmount || document.documentElement.scrollTop > scrollAmount) {
        document.getElementsByTagName("header")[0].classList.add("scrolled")
        if(scrolltopbtn)
        scrolltopbtn.style.transform = "scale(1)"
      } else {
        document.getElementsByTagName("header")[0].classList.remove("scrolled")
        if(scrolltopbtn)
        scrolltopbtn.style.transform = "scale(0)"
      }
    }

    function toggleMenu() {
      let item = document.getElementsByClassName('sz-navbar-inner')[0]
      $(item).toggleClass('active')
      $('body').css('overflow-y', 'hidden')
    }

    function getStartStep() {
      let start = document.getElementsByClassName("informationSection__steppers__start")[0]
      let content = document.getElementsByClassName("informationSection__steppers__content")[0]
      $(start).hide()
      $(content).show() 
    }
